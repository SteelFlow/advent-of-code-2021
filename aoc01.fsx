open System.IO

let example =
    [ 199
      200
      208
      210
      200
      207
      240
      269
      260
      263 ]

let input =
    File.ReadAllLines("input01.aoc")
    |> Array.map (fun x -> (int x))
    |> Array.toList

let rec countIncrements (curentCount: int) (input: int list) =
    match input.Length with
    | x when x > 1 ->
        match input.Head < input.Tail.Head with
        | true -> countIncrements (curentCount + 1) input.Tail
        | false -> countIncrements curentCount input.Tail
    | _ -> curentCount

let rec toTriplets (triplets: (int * int * int) list) (input: int list) =
    match input.Length with
    | x when x > 2 ->
        input.Tail
        |> toTriplets (
            (input.Head, input.Tail.Head, input.Tail.Tail.Head)
            :: triplets
        )
    | _ -> triplets |> List.rev

let addTriplets (n1, n2, n3) = n1 + n2 + n3

printfn "%d" (countIncrements 0 input)

let addedTriplets =
    (toTriplets [] input) |> List.map addTriplets

printfn "%d" (countIncrements 0 addedTriplets)
